﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MapMe.Startup))]
namespace MapMe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
